package net.sixdeex.udemy.ktwit.es;

import static java.lang.String.format;
import static java.time.Instant.now;

import java.io.IOException;
import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;

import org.apache.http.HttpHost;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.Requests;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

class ESPushTest {
	private static final RestHighLevelClient CLIENT = new RestHighLevelClient(RestClient.builder(new HttpHost("localhost", 9200, "http")));

	@AfterAll
	static void afterAll() throws IOException {
		CLIENT.close();
	}

	@Test
    void testBulkSynch() throws IOException {
		
		Bean b = new Bean();
		String doc = new Gson().toJson(b, new TypeToken<Bean>() {}.getType());
		
    	BulkRequest br = new BulkRequest();
    	br.add(Requests.indexRequest("test_idx").source(doc, XContentType.JSON).id(format("%s-%d",UUID.randomUUID(),b.now.getNano())));
    	
    	CLIENT.bulk(br, RequestOptions.DEFAULT);
    }

	@Test
    void testBulkAsynch() throws IOException, InterruptedException {

		Bean b = new Bean();
		String doc = new Gson().toJson(new Bean(), new TypeToken<Bean>() {}.getType());
		
    	BulkRequest br = new BulkRequest();
    	br.add(Requests.indexRequest("test_idx").source(doc, XContentType.JSON).id(format("%s-%d",UUID.randomUUID(),b.now.getNano())));
    	
    	CountDownLatch latch = new CountDownLatch(1);
    	CLIENT.bulkAsync(br, RequestOptions.DEFAULT, new ActionListener<BulkResponse>() {
			
			@Override
			public void onResponse(BulkResponse response) {
				latch.countDown();
			}
			
			@Override
			public void onFailure(Exception e) {
				latch.countDown();
			}
		});
    	latch.await();
    }

	public static final class Bean implements Serializable {
		private static final long serialVersionUID = 1L;
		String p1 = "Hello world";
		Instant now = now();
		public String getP1() {
			return p1;
		}
		public Instant getNow() {
			return now;
		}
	}
}
