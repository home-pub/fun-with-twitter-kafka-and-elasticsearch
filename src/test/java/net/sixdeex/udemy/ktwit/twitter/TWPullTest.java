package net.sixdeex.udemy.ktwit.twitter;

import static java.lang.String.format;

import org.junit.jupiter.api.Test;

import twitter4j.Twitter;
import twitter4j.TwitterFactory;

public class TWPullTest {

	@Test
	void testGetHomeTimeline() throws Exception {

		Twitter instance = TwitterFactory.getSingleton();
		instance.getHomeTimeline().forEach(status -> {
			System.out.println(format("%s: %s", status.getUser().getName(), status.getText()));
		});
	}
}
