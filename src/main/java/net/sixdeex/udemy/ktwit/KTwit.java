package net.sixdeex.udemy.ktwit;

import static java.time.Duration.ofMillis;
import static java.util.Collections.singleton;
import static org.apache.kafka.clients.consumer.ConsumerConfig.AUTO_OFFSET_RESET_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.GROUP_ID_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG;
import static org.apache.kafka.clients.producer.ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG;
import static org.apache.kafka.clients.producer.ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpHost;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.Requests;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

public class KTwit implements Runnable {
	private static final RestHighLevelClient ES_CLIENT = new RestHighLevelClient(
			RestClient.builder(new HttpHost("localhost", 9200, "http")));

	private static final String BOOTSTRAP_SERVER = "127.0.0.1:9092";
	private static final String TOPIC = "twitter";
	private static final String ES_INDEX = "twitter";
	private static final String GROUP_ID = "es";
	private static final Type TWEET_TYPE = new TypeToken<Tweet>() {
	}.getType();
	private static final Gson GSON = new Gson();

	public static void main(String[] args) throws TwitterException, InterruptedException {

		Phaser phaser = new Phaser(2);
		KTwit ktwit = new KTwit(phaser);

		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			System.out.println("Stopping consumer");
			CountDownLatch latch = new CountDownLatch(1);
			ktwit.stop(latch);
			try {
				ES_CLIENT.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				latch.await(10, TimeUnit.SECONDS);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		}));

		Properties props = new Properties();
		props.setProperty(BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVER);
		props.setProperty(KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		props.setProperty(VALUE_SERIALIZER_CLASS_CONFIG, TweetSerializer.class.getName());

		new Thread(ktwit).start();
		phaser.arriveAndAwaitAdvance();

		System.out.println("Start fetching tweets");
		Twitter instance = TwitterFactory.getSingleton();
		try (KafkaProducer<String, Tweet> producer = new KafkaProducer<>(props)) {
			instance.getHomeTimeline().stream()
					.map(status -> new ProducerRecord<>(TOPIC, status.getUser().getName(), new Tweet(status)))
					.forEach(producer::send);
		}
		System.out.println("End of data");
	}

	private Phaser phaser;
	private CountDownLatch latch;
	private volatile boolean running = true;

	public KTwit(Phaser phaser) {
		this.phaser = phaser;
	}

	public void stop(CountDownLatch latch) {
		running = false;
		this.latch = latch;
	}

	@Override
	public void run() {
		Properties props = new Properties();
		props.setProperty(BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVER);
		props.setProperty(KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		props.setProperty(VALUE_DESERIALIZER_CLASS_CONFIG, TweetDeserializer.class.getName());
		props.setProperty(GROUP_ID_CONFIG, GROUP_ID);
		props.setProperty(AUTO_OFFSET_RESET_CONFIG, "latest");

		try (KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props)) {
			consumer.subscribe(singleton(TOPIC));

			System.out.println("Consumer ready, start listening");
			phaser.arriveAndAwaitAdvance();
			while (running) {
				ConsumerRecords<String, String> records = consumer.poll(ofMillis(100));
				final BulkRequest request = new BulkRequest();
				records.forEach(record -> {
					String doc = GSON.toJson(record.value(), TWEET_TYPE);
					request.add(Requests.indexRequest(ES_INDEX).source(doc, XContentType.JSON));
				});
				ES_CLIENT.bulkAsync(request, RequestOptions.DEFAULT, new ActionListener<BulkResponse>() {
					@Override
					public void onResponse(BulkResponse response) {
					}

					@Override
					public void onFailure(Exception e) {
					}
				});
			}
		}

		System.out.println("Consumer stopped");
		latch.countDown();
	}

	public static class TweetSerializer implements Serializer<Tweet> {
		@Override
		public byte[] serialize(String topic, Tweet data) {
			try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
				new ObjectOutputStream(baos).writeObject(data);
				return baos.toByteArray();
			} catch (IOException e) {
				return new byte[] {};
			}
		}
	}

	public static class TweetDeserializer implements Deserializer<Tweet> {
		@Override
		public Tweet deserialize(String topic, byte[] data) {
			try (ByteArrayInputStream bais = new ByteArrayInputStream(data)) {
				return (Tweet) new ObjectInputStream(bais).readObject();
			} catch (ClassNotFoundException | IOException e) {
				return null;
			}
		}
	}

	public static class Tweet implements Serializable {
		private static final long serialVersionUID = 1L;
		private String user;
		private String text;
		private Date ts;

		public Tweet(Status tweet) {
			user = tweet.getUser().getName();
			text = tweet.getText();
			ts = tweet.getCreatedAt();
		}

		public String getUser() {
			return user;
		}

		public void setUser(String user) {
			this.user = user;
		}

		public String getText() {
			return text;
		}

		public void setText(String text) {
			this.text = text;
		}

		public Date getTs() {
			return ts;
		}

		public void setTs(Date ts) {
			this.ts = ts;
		}
	}
}
